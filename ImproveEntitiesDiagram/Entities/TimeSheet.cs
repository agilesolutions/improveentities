﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImproveEntitiesDiagram.Entities
{
    public class TimeSheet
    {
        public int Year { get; set; }
        public int Month { get; set; }

        public Employee Employee { get; set; }


    }
}
