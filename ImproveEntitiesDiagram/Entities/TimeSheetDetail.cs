﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImproveEntitiesDiagram.Entities
{
    public abstract class TimeSheetDetail
    {
        public TimeSheet TimeSheet { get; set; }
        public int Day { get; set; }
        
        public decimal Hours { get; set; }
    }
}
