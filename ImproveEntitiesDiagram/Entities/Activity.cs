﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImproveEntitiesDiagram.Entities
{
    public class Activity
    {
        public Activity Parent { get; set; }
        public Project Project { get; set; }
    }
}
